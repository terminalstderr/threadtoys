/*
2014 -- Ryan Leonard
deadlock.c is an example of a program that is contrived to reach a deadlock
between two threads based on their access to two resources. The resources are
emulated by use of pthread_mutex construct.
*/

#define _MULTI_THREADED
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

#include <pthread.h>

void *run_thread1(void *);
void *run_thread2(void *);

  // Define these globally for lazy access from run_thread[] methods.
pthread_mutex_t 
      mutex1,
      mutex2;

int main (int argc, char **argv) {

  // Create the mutex(es?)
  pthread_mutex_init(&mutex1, NULL);
  pthread_mutex_init(&mutex2, NULL);
  
  // Create the threads
  pthread_t t1, t2;
  pthread_create(&t1, NULL, run_thread1, NULL);
  pthread_create(&t2, NULL, run_thread2, NULL);

  // Wait to join the threads
  pthread_join(t1, NULL);
  pthread_join(t2, NULL);

  // Finished -- no cleanup to do
  exit(EXIT_SUCCESS);
}


/*
The whole job of this function is to take a resource, wait for a moment, then 
try to take another resource without releasing the first resource.
*/
void *run_thread1(void *null){

  // allocate resource 1 
  pthread_mutex_lock(&mutex1);
  printf("Acquired resource 1 in thread 1.\n");

  // allocate resource 2
  pthread_mutex_lock(&mutex2);
  printf("Acquired resource 2 in thread 1.\n");

  pthread_mutex_unlock(&mutex1);
  pthread_mutex_unlock(&mutex2);

  pthread_exit(NULL);
}

void *run_thread2(void *null){

  usleep(1);
  // allocate resource 2
  pthread_mutex_lock(&mutex2);
  printf("Acquired resource 2 in thread 2.\n");

  // allocate resource 1
  pthread_mutex_lock(&mutex1);
  printf("Acquired resource 1 in thread 2.\n");

  pthread_mutex_unlock(&mutex2);
  pthread_mutex_unlock(&mutex1);

  pthread_exit(NULL);
}
