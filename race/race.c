/*
2014 -- Ryan Leonard
This is a generic toy example which demonstrates that order of scheduling
when dealing with multithreading does not equal order of completion. 
*/
#define _MULTI_THREADED
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

// XXX modify these values to play with the variables computation time XXX
const int AVAL = 0;
const int BVAL = 0;
const int CVAL = 0;
const int DVAL = 999999;
const int MAXVAL = 1000000;

// A generic structure that has a human readable ID and a value
struct named_int {
  char id;
  int val;
};

void *sync_computation(void *);

int main(int argc, char **argv) {

  // Create the structures to be computed on
  struct named_int a, b, c, d;   
  a.val = AVAL; a.id = 'a';
  b.val = BVAL; b.id = 'b';
  c.val = CVAL; c.id = 'c';
  d.val = DVAL; d.id = 'd';
  printf("Prior Computation %c: %d\n", a.id, a.val);
  printf("Prior Computation %c: %d\n", b.id, b.val);
  printf("Prior Computation %c: %d\n", c.id, c.val);
  printf("Prior Computation %c: %d\n", d.id, d.val);

  // Create the threads 
  pthread_t inc_a, inc_b, inc_c, inc_d;
  if (pthread_create(&inc_a, NULL, sync_computation, &a)) {
    fprintf(stderr, "Error creating thread\n");
    return 1;
  } 
  if (pthread_create(&inc_b, NULL, sync_computation, &b)) {
    fprintf(stderr, "Error creating thread\n");
    return 1;
  } 
  if (pthread_create(&inc_c, NULL, sync_computation, &c)) {
    fprintf(stderr, "Error creating thread\n");
    return 1;
  } 
  if (pthread_create(&inc_d, NULL, sync_computation, &d)) {
    fprintf(stderr, "Error creating thread\n");
    return 1;
  } 

  // Performing computations
  /*
    Do nothing in main/parent thread, everything is being performed by threads
  */

  // Wait for each thread to finish its computation
  if(pthread_join(inc_a, NULL)) {
    fprintf(stderr, "Error joining thread\n");
    return 2;
  }
  if(pthread_join(inc_b, NULL)) {
    fprintf(stderr, "Error joining thread\n");
    return 2;
  }
  if(pthread_join(inc_c, NULL)) {
    fprintf(stderr, "Error joining thread\n");
    return 2;
  }
  if(pthread_join(inc_d, NULL)) {
    fprintf(stderr, "Error joining thread\n");
    return 2;
  }
 
  // All resources have been cleaned; exit the program  
  exit(EXIT_SUCCESS);
}

/*
This is a toy computation that takes some processing cycles.
Description: sync_computation takes a named integer, adds the value 1 to it
  until it has reached the value MAXVAL, specified at start of this file.
*/
void *sync_computation(void *param){

  // Retype the parameter to keep compiler happy
  struct named_int *ptr = (struct named_int*) param;

  // Perform `computation'
  while (ptr->val < MAXVAL)
    ptr->val = ptr->val + 1;

  printf("Computation complete %c: %d\n", ptr->id, ptr->val);

  // This function has a void* return type -- throw back NULL
  return NULL;
}
