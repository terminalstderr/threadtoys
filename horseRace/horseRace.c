/*
2014 -- Ryan Leonard
This is a toy that will programatically, dynamically create and handle threads
as they perform some simple computation.
*/
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>

#include <fcntl.h>
#include <unistd.h>

// XXX modify these values to play with the variables computation time XXX
const int MAX_NAME_LEN = 32;
const int MAXVAL = 1000000;
const char *racerCsv = "./horseNames.csv";
int racerCsvFd = 0;

// A generic structure that has a human readable ID and a value
struct named_int {
  char id[32];
  int val;
};

void chsRacerName(char *name);
void *sync_computation(void *);

int main(int argc, char **argv) {
  
  int
    racerCount = 0,
    i = 0;

  char 
    *endptr, 
    *str;

  struct named_int 
    *namedIntList,
    *cur_val;

  pthread_t 
    *threadList,
    *cur_thread;
  
  // Get input from command line
  if (argc != 2) {
    printf("usage: %s <racer-count>", argv[0]);
    exit(EXIT_FAILURE);
  }
  str = argv[1];
  errno = 0;
  racerCount = strtol(str, &endptr, 10);
  if (errno != 0) {
    perror("strtol");
    exit(EXIT_FAILURE);
  }
  if (endptr == str) {
    fprintf(stderr, "No digits were found\n");
    exit(EXIT_FAILURE);
  }  
  if (racerCount > 100 || racerCount < 1) {
    fprintf(stderr, "Invalid racer-count parameter: %d\n", racerCount);
    exit(EXIT_FAILURE);
  }

  // Create a list of structures and meta-data for the threads
  errno = 0;
  namedIntList = malloc(sizeof(struct named_int) * racerCount);
  threadList = malloc(sizeof(pthread_t) * racerCount);
  if (namedIntList == NULL || threadList == NULL || errno != 0) {
    fprintf(stderr, "Could not malloc memory\n");
    exit(EXIT_FAILURE);
  }
  // initialize all of the racers
  for (i = 0 ; i < racerCount ; i++) {
    cur_val = namedIntList + i;
    cur_val->val = 0;
    chsRacerName(cur_val->id);
    printf("Racer %s starting at position: %d \n", cur_val->id, cur_val->val);
  }

  // Create the threads 
  for (i = 0 ; i < racerCount ; i++) {
    cur_val = namedIntList + i;
    cur_thread = threadList + i;
    if (pthread_create(cur_thread, NULL, sync_computation, cur_val)) {
      fprintf(stderr, "Error creating thread\n");
      exit(EXIT_FAILURE);
    } 
  }

  // Performing computations
  /*
    Do nothing in main/parent thread, everything is being performed by threads
  */

  // Wait for each thread to finish its computation
  for (i = 0 ; i < racerCount ; i++) {
    cur_thread = threadList + i;
    if(pthread_join(*cur_thread, NULL)) {
      fprintf(stderr, "Error joining thread\n");
      exit(EXIT_FAILURE);
    }
  }

  // Cleanup memory from structs and thread pointers
  free(namedIntList);
  free(threadList);

  // All resources have been cleaned; exit the program  
  exit(EXIT_SUCCESS);
}

/*
This is a toy computation that takes some processing cycles.
Description: sync_computation takes a named integer, adds the value 1 to it
  until it has reached the value MAXVAL, specified at start of this file.
*/
void *sync_computation(void *param){

  // Retype the parameter to keep compiler happy
  struct named_int *ptr = (struct named_int*) param;

  // Perform `computation'
  while (ptr->val < MAXVAL)
    ptr->val = ptr->val + 1;

  printf("Racer %s finished! (%d)\n", ptr->id, ptr->val);

  // This function has a void* return type -- throw back NULL
  return NULL;
}

/*
A toy function to choose a racer's name from a file provided
- char *name must point to a free 32 char byte string.
- This function must be called less times than there are names.
*/
void chsRacerName(char *name) {
  int 
    ret,
    i;
  // If the file is not yet opened, open it 
  if (racerCsvFd == 0) {
    racerCsvFd = open(racerCsv, O_RDONLY);
    if (racerCsvFd == -1) {
      fprintf(stderr, "Could not open file: %s\n", racerCsv);
      exit(EXIT_FAILURE);
    }
  }

  // Read the next name from the list
  ret = read(racerCsvFd, name, MAX_NAME_LEN);
  if (ret < 1) {
    fprintf(stderr, "Could not read from file\n");
    exit(EXIT_FAILURE);
  }
  for (i = 0 ; i < MAX_NAME_LEN ; i++){
    if (name[i] == ',') {
      name[i] = '\0';
      break;
    }
  }
  lseek(racerCsvFd, -(MAX_NAME_LEN-i-2), SEEK_CUR);
}
